package com.example.btgfake.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.btgfake.HttpConfiguration.ShopObjects;
import com.example.btgfake.MainActivity;
import com.example.btgfake.R;

import java.util.ArrayList;
import java.util.List;

public class CardProductsAdapter extends RecyclerView.Adapter<CardProductsAdapter.MyViewHolder> implements Filterable {

    private List<ShopObjects> shopObjects;
    private List<ShopObjects> shopObjectsFull;
    private OnItemClickListener mListener;

    public List<ShopObjects> getShopObjects() {
        return shopObjects;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public CardProductsAdapter(MainActivity mainActivity, List<ShopObjects> shopObjects) {
        this.shopObjects = shopObjects;
        this.shopObjectsFull = new ArrayList<>(shopObjects);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_products, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tx_productBrand.setText(shopObjects.get(position).getBrand());
        holder.tx_productName.setText(shopObjects.get(position).getProductName());
        Context context = holder.itemView.getContext();

        Glide.with(context).load(shopObjects.get(position).getImage()).into(holder.iv_productImage);
    }

    @Override
    public int getItemCount() {
        return shopObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tx_productBrand, tx_productName;
        public ImageView iv_productImage;

        public MyViewHolder(View view) {
            super(view);
            tx_productBrand = (TextView) view.findViewById(R.id.tv_productBrand);
            tx_productName = (TextView) view.findViewById(R.id.tv_productName);
            iv_productImage = (ImageView) view.findViewById(R.id.iv_productImage);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return shopObjectsFilter;
    }

    private Filter shopObjectsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ShopObjects> filteredShopObjects = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredShopObjects.addAll(shopObjectsFull);
            } else {
                String filteredObjects = constraint.toString().toLowerCase().trim();

                for (ShopObjects shopObjects : shopObjectsFull) {
                    if (shopObjects.getProductName().toLowerCase().contains(filteredObjects)) {
                        filteredShopObjects.add(shopObjects);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredShopObjects;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            shopObjects.clear();
            shopObjects.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
