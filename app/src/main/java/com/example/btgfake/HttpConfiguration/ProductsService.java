package com.example.btgfake.HttpConfiguration;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ProductsService {
    @GET("product")
    Call<List<ShopObjects>> getProductId();
}
