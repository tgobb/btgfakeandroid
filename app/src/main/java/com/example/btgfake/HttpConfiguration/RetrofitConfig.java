package com.example.btgfake.HttpConfiguration;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;

    public RetrofitConfig () {
        this.retrofit = new Retrofit.Builder()
                .baseUrl("https://sale-attribution.allin.com.br/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public ProductsService getProductId() {
        return this.retrofit.create(ProductsService.class);
    }
}
