package com.example.btgfake.HttpConfiguration;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopObjects {
    @SerializedName("product_id")
    @Expose
    private String productId;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("installment_value")
    @Expose
    private String installmentValue;

    @SerializedName("price_by")
    @Expose
    private String priceBy;

    @SerializedName("price_by_num")
    @Expose
    private String priceByNum;

    @SerializedName("product_desc")
    @Expose
    private String productDesc;

    @SerializedName("product_link")
    @Expose
    private String productLink;

    @SerializedName("product_name")
    @Expose
    private String productName;

    @SerializedName("sub_category")
    @Expose
    private String subCategory;

    public String getProductId() {
        return productId;
    }

    public String getBrand() {
        return brand;
    }

    public String getCategory() {
        return category;
    }

    public String getImage() {
        return image;
    }

    public String getInstallmentValue() {
        return installmentValue;
    }

    public String getPriceBy() {
        return priceBy;
    }

    public String getPriceByNum() {
        return priceByNum;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public String getProductLink() {
        return productLink;
    }

    public String getProductName() {
        return productName;
    }

    public String getSubCategory() {
        return subCategory;
    }
}
