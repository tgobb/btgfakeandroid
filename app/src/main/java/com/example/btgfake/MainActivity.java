package com.example.btgfake;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.btgfake.Adapters.CardProductsAdapter;
import com.example.btgfake.HttpConfiguration.RetrofitConfig;
import com.example.btgfake.HttpConfiguration.ShopObjects;

import java.util.List;

import br.com.allin.mobile.pushnotification.AlliNPush;
import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AISearch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements CardProductsAdapter.OnItemClickListener {
    public static final String DETAIL_PRODUCT_BRAND ="clickedProductBrand";
    public static final String DETAIL_PRODUCT_NAME ="clickedProductName";
    public static final String DETAIL_PRODUCT_DESC ="clickedProductDescription";
    public static final String DETAIL_PRODUCT_IMAGE ="clickedProductImage";
    public static final String DETAIL_PRODUCT_ID ="clickedProductId";


    private RecyclerView recyclerView;
    private CardProductsAdapter cardProductsAdapter;
    private ShopObjects shopObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AlliNPush.getInstance().registerForPushNotifications(this);

        Log.d("debug", AlliNPush.getInstance().getDeviceToken());

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF9027")));
        bar.setTitle("BTG");

        recyclerView = findViewById(R.id.rv_Products);


        Call<List<ShopObjects>> call = new RetrofitConfig().getProductId().getProductId();

        call.enqueue(new Callback<List<ShopObjects>>() {
            @Override
            public void onResponse(Call<List<ShopObjects>> call, Response<List<ShopObjects>> response) {
                // pegar a resposta
                List<ShopObjects> shopObjects = response.body();
                cardProductsAdapter = new CardProductsAdapter(MainActivity.this, shopObjects);

                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MainActivity. this, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(cardProductsAdapter);

                cardProductsAdapter.setOnItemClickListener(MainActivity.this);
            }

            @Override
            public void onFailure(Call<List<ShopObjects>> call, Throwable t) {
                Log.e("erro:", "erro:" + t.getMessage());
            }
        });

        Button btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                cardProductsAdapter.getFilter().filter(newText);
                AISearch aiSearch = new AISearch((newText));
                BTG360.addSearch("60:1", aiSearch);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onItemClick(int position) {
        Intent productDetail = new Intent(this, ProductDetails.class);
        ShopObjects shopObjects = cardProductsAdapter.getShopObjects().get(position);

        productDetail.putExtra(DETAIL_PRODUCT_NAME, shopObjects.getProductName());
        productDetail.putExtra(DETAIL_PRODUCT_BRAND, shopObjects.getBrand());
        productDetail.putExtra(DETAIL_PRODUCT_DESC, shopObjects.getProductDesc());
        productDetail.putExtra(DETAIL_PRODUCT_IMAGE, shopObjects.getImage());
        productDetail.putExtra(DETAIL_PRODUCT_ID, shopObjects.getProductId());

        startActivity(productDetail);
    }
}
