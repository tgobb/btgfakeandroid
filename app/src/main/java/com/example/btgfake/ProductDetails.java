package com.example.btgfake;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AIWarn;
import br.com.allin.mobile.pushnotification.entity.btg.AIWish;

import static com.example.btgfake.MainActivity.DETAIL_PRODUCT_BRAND;
import static com.example.btgfake.MainActivity.DETAIL_PRODUCT_DESC;
import static com.example.btgfake.MainActivity.DETAIL_PRODUCT_ID;
import static com.example.btgfake.MainActivity.DETAIL_PRODUCT_IMAGE;
import static com.example.btgfake.MainActivity.DETAIL_PRODUCT_NAME;

public class ProductDetails extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF9027")));
        bar.setTitle("BTG");

        Intent intent = getIntent();
        String productImage = intent.getStringExtra(DETAIL_PRODUCT_IMAGE);
        String productBrand = intent.getStringExtra(DETAIL_PRODUCT_BRAND);
        String productName = intent.getStringExtra(DETAIL_PRODUCT_NAME);
        String productDescription = intent.getStringExtra(DETAIL_PRODUCT_DESC);
        final String productId = intent.getStringExtra(DETAIL_PRODUCT_ID);

        ImageView imageView = findViewById(R.id.iv_detailProductImage);
        TextView tv_productBrand = findViewById(R.id.tv_detailProductBrand);
        TextView tv_productName = findViewById(R.id.tv_detail_productName);
        TextView tv_productDescription = findViewById(R.id.tv_detailProductDescription);

        tv_productBrand.setText(productBrand);
        tv_productDescription.setText(productDescription);
        tv_productName.setText(productName);

        Glide.with(this).load(productImage).into(imageView);

        Button btnwishList = findViewById(R.id.btn_wishList);
        Button btnWarnme = findViewById(R.id.btn_warnMe);

        btnwishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProductDetails.this,"Seu produto foi adicionado a lista de desejos!", Toast.LENGTH_LONG).show();

                AIWish aiWish = new AIWish(productId, true);
                BTG360.addWishList("60:1", aiWish);
            }
        });

        btnWarnme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProductDetails.this,"Quanado o produto estiver disponível iremos avisá-lo(a).", Toast.LENGTH_LONG).show();

                AIWarn aiWarn = new AIWarn(productId, true);
                BTG360.addWarnMe("60:1", aiWarn);
            }
        });


    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.backbutton, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.backbutton) {
            // do something here
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
